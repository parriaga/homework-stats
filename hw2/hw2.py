import matplotlib.pyplot as plot
import matplotlib.cm as cm
import matplotlib.image as mpimg
import scipy.optimize as oppy
import pyfits
import numpy
from matplotlib.colors import LogNorm

def fitfun_errors(p, x, y):
    '''
    Fits a line to data which has a 30 percent chance of having a
    systematic additive error of 5
    inputs: p containing m, b
    x and y data
    outputs: the likelihood function
    '''
    m, b = p
    arg1 = -.5 * (y - m*x - b)**2. 
    arg2 = -.5 * (y - m*x - b - 5.)**2. 
    sigma = 1.
    denom = numpy.sqrt(2. * numpy.pi) * sigma
    likeli = .7 * numpy.exp(arg1) + .3  * numpy.exp(arg2)
    # Want to maximize likelihood so return negative
    pfunc = -numpy.prod(likeli)
    return pfunc
    



def fitfun_noerrors(p, x, y):
    '''
    Returns sum of residuals for chi square fitting
    '''
    m, b = p
    resids = numpy.sum((y - m * x - b)**2.)
    return resids



def plot2():
    # Unused in homework
    ms = [0.1, .78, -.11,.23, -.65, -.96]
    bs = [0.1, 1.4, 9., 3.5, 11., 11.4]
    data = numpy.loadtxt('hw2prob1-data.txt')
    #True for plus 5
    x = data[:, 0] 
    y = data[:, 1]
    plot.scatter(x, y)
    plot.scatter(x, y + 5., color = 'g', s= 20)
    xarr = numpy.arange(12.)
    for i in range(len(ms)):
        minit = ms[i]
        binit = bs[i]
        mfi, bfi, finalval = fit_fmin(initconds = [minit, binit])
        plot.plot(xarr ,xarr*mfi + bfi + 5,  label = 'Likelihood: ' + str(finalval)[:5] + ' ' + str(i), linewidth = 2.0)
    shittyvals = fit_fmin(ignore_error = True)
    mnoerr, bnoerr, finalval = shittyvals
    plot.legend()
    plot.show()


def doallplot():
    '''
    Makes plots for fits with various initial conditions
    '''
    ms = [.78, -.08,.23, -.65, -0.08,-.96]
    bs = numpy.array([ 1.4, 9., 3.5, 11., 4.0, 11.4]) 
    data = numpy.loadtxt('hw2prob1-data.txt')
    #Sets which data points are on and off for various fits
    booltable = [[0., 5., 5.],
                 [5., 5., 5.],
                 [0., 5., 0.],
                 [5., 5., 0.],
                 [0., 0., 0.],
                 [5., 5., 0.]]
    x = data[:, 0] 
    y = data[:, 1]
    plot.scatter(x, y)
    plot.scatter(x, y + 5., color = 'g', s= 20)
    xarr = numpy.arange(12.)
    for i in range(len(ms)):
        minit = ms[i]
        binit = bs[i]
        mfi, bfi, finalval = fit_fmin(initconds = [minit, binit])
        plot.subplot('32' + str(i+1))
        plot.title('Peak ' + str(i+1) + '   Likelihood: ' + str(finalval)[0:6])
        plot.gray()
        plot.plot(xarr ,xarr*mfi + bfi + 5, linewidth = 2.0)
        plot.errorbar(x, y + booltable[i], yerr = 1., fmt = None)
        if i < 4:
            plot.tick_params(
                axis='x',          # changes apply to the x-axis
                which='both',      # both major and minor ticks are affected
                bottom='off',      # ticks along the bottom edge are off
                top='off',         # ticks along the top edge are off
                labelbottom='off') # labels along the bottom edge are off
        if i == 1:
            plot.scatter(x, y + booltable[i], c = 'w', s = 40)
        else:
            plot.scatter(x, y + booltable[i], c = booltable[i], s = 40)
        plot.xlim([0, 10])
        plot.ylim([5, 18])
    plot.show()
    plot.errorbar(x, y, yerr = 1.0, fmt = 'o')
    noerrvals = fit_fmin(ignore_error = True)
    mnoerr, bnoerr, finalval = noerrvals
    plot.plot(xarr, xarr * mnoerr + bnoerr, linewidth = 2.0)
    plot.title('Fit Ignoring Errors')
    plot.show()
    plot.title('Initial Conditions = 0.1, 0.1')
    plot.errorbar(x, y, yerr = 1.0, fmt = 'o', markerfacecolor = 'g')
#    plot.errorbar(x, y + 5, yerr = 1.0, fmt = 'o', markerfacecolor = 'r', label = 'original data points + 5')
    mfi, bfi, finalval = fit_fmin(initconds = [0.1, 0.1])
    plot.plot(xarr ,xarr*mfi + bfi + 5, linewidth = 2.0)
    plot.show()
    




def fit_fmin(initconds = [.1, .1], ignore_error = False):
    '''
    Takes initial conditions, returns slope, offset and the likelihood value
    '''
    data = numpy.loadtxt('hw2prob1-data.txt')
    x = data[:, 0] 
    y = data[:, 1]
    if ignore_error is True:
        fitfunc = fitfun_noerrors
    else:
        fitfunc = fitfun_errors
    pfinal = oppy.fmin(fitfunc, initconds, args = (x, y))
    m, b = pfinal
    finalval = -fitfun_errors(pfinal, x, y)
    return m, b, finalval


def get_likelihood(initconds = [.1, .1], ignore_error = False):
    '''
    Just returns likelihood for a set of parameters
    '''
    data = numpy.loadtxt('hw2prob1-data.txt')
    x = data[:, 0] 
    y = data[:, 1]
    finalval = -fitfun_errors(initconds, x, y)
    return finalval




def likelihood_contour():
    '''
    Contour plot of the likelihood as a function of m and b
    '''
    sample = 5.
    # M matrix
    ran1 = numpy.arange(-1.5, 1.5, .01)
    # B matrix
    ran2 = numpy.arange(-5., 15.,.05)
    parray = numpy.zeros((len(ran1), len(ran2)))
    for i in range(len(ran1)):
        for j in range(len(ran2)):
            pinits = [ran1[i], ran2[j]]
            pval = get_likelihood(pinits)
            parray[i, j] = pval
#    a1_arr, a2_arr = numpy.meshgrid(ran1, ran2)
#    a1_arr = a1_arr.flatten()
#    a2_arr = a2_arr.flatten()
    emin, emax = numpy.e**(numpy.min(parray)), numpy.e**(numpy.max(parray))
#    lognorm = numpy.log(numpy.arange(emin, emax, (emax-emin) / 20.))
    plot.contourf(ran2, ran1, parray, 100, cmap = cm.hot)
#    pyfits.writeto('parray.fits', parray)
    plot.colorbar()
    plot.contour(ran2, ran1, parray, 100, color = 'b')
    plot.xlabel('a0')
    plot.ylabel('a1')
    plot.show()
        
    

def superghetto():
    '''
    i gave up on making nice plot
    lognorm in matplotlib makes me cry
    so does ds9 plot axes
    i spent an hour on this plot
    why 
    '''
    im = mpimg.imread('ghetto.png')
    plot.imshow(im, extent =[-5, 15, -1.5, 1.5], aspect = 'normal', origin = 'upper')
    plot.title('Likelihood Map')
    plot.xlabel('$b$')
    plot.ylabel('$m$')
    plot.show()

    


