import scipy.special as sp
import matplotlib.pyplot as plot
import numpy


def q2(n_arr = [3.,6], m = numpy.arange(11 * 50.) / 50.):
    '''
    Plots the probability of detecting M photons given that a exposure
    of the same length found N photons
    Input: N, M
    '''
    linestyles = ['-', '--', ':']
    for i, n in enumerate(n_arr):
        p_mn = (sp.gamma(m + n + 1.))  / (sp.gamma( n + 1) * sp.gamma(m + 1))
        p_mn = p_mn / (2.**(m + n + 1.))
        plot.plot(m, p_mn, label = 'N = ' + str(n), linestyle =
                  linestyles[i], linewidth = 2)
    plot.xlabel('M')
    plot.ylabel('$P(M|N)$')
    plot.legend()
    plot.show()
    plot.savefig('q2.png')
