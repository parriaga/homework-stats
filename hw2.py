import matplotlib.pyplot as plot
import scipy.optimize as oppy
import numpy


def fitfun_errors(p, x, y):
    '''
    Fits a line to data which has a 30 percent chance of having a
    systematic additive error of 5
    inputs: p containing m, b
    x and y data
    outputs: the product of the likelihood function
    '''

    m, b = p
    arg1 = -.5 * (y - m*x - b)**2. 
    arg2 = -.5 * (y - m*x - b - 5.)**2. 
#    denom = numpy.sqrt(2. * numpy.pi) * sigma
    likeli = .7 * numpy.exp(arg1) + .3  * numpy.exp(arg2)
    # Want to maximize likelihood so return negative
    pfunc = -numpy.prod(likeli)
    return pfunc
    



def fitfun_noerrors(p, x, y):
    m, b = p
    resids = numpy.sum((y - m * x - b)**2.)
    return resids


def fitthing(initconds = [.1, .1], ignore_error = False):
    data = numpy.loadtxt('hw2prob1-data.txt')
    x = data[:, 0] 
    y = data[:, 1]
    if ignore_error is True:
        fitfunc = fitfun_noerrors
    else:
        fitfunc = fitfun_errors
    print initconds
#    pfinal = oppy.fmin(fitfunc, initconds, args = (x, y))
#    m, b = pfinal
    finalval = -fitfun_errors(initconds, x, y)
    return finalval


def runthrough():
    sample = 5.
    ran1 = numpy.arange(-1.5, 1.5, .01)
    ran2 = numpy.arange(-5., 15.,.05)
    parray = numpy.zeros((len(ran1), len(ran2)))
    for i in range(len(ran1)):
        for j in range(len(ran2)):
            pinits = [ran1[i], ran2[j]]
            pval = fitthing(pinits)
            parray[i, j] = pval
#    a1_arr, a2_arr = numpy.meshgrid(ran1, ran2)
#    a1_arr = a1_arr.flatten()
#    a2_arr = a2_arr.flatten()
    plot.contourf(ran2, ran1, parray, 50)
    plot.contour(ran2, ran1, parray, 30)
    plot.xlabel('a0')
    plot.ylabel('a1')
    plot.show()
        
    





