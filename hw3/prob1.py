import pyfits
import numpy
import matplotlib.pyplot as plot
import csv
from scipy.optimize import leastsq
import numpy.linalg as lin




dat = pyfits.getdata('hw3prob1-data.fits.gz')
imsize = numpy.shape(dat)



def tdgauss(p = [30., 30., 100., 4., 100]):
    x0, y0,a, sigma, c = p
    x,y = numpy.meshgrid(numpy.arange(imsize[0]), numpy.arange(imsize[1]))
    gauss = a * numpy.exp(-.5 *((x - x0)**2. + (y - y0)**2.) / sigma**2.) + c
    return gauss

def tdgauss_cov(p = [30., 30., 100., 4., 5., 100]):
    x0, y0,a, sigmax, sigmay, c = p
    x,y = numpy.meshgrid(numpy.arange(imsize[0]), numpy.arange(imsize[1]))
    gauss = a * numpy.exp(-.5 *((x - x0)**2. + (y - y0)**2.) / sigma**2.) + c
    return gauss


def residuals(p, im):
    td = tdgauss(p)
    residmap = (im - td)**2. / (im / 3.)**2.
    return residmap.flatten()


def fitter():
    initparms = [120., 120., 100., 4., 100.]
    pfinal = leastsq(residuals, initparms, args = (dat), full_output = True)
    params = pfinal[0]
    print params
    cov = pfinal[1]
    medval = numpy.median(dat[0:50, 0:50])
    print 'chisq'
    print numpy.sum(residuals(pfinal[0], dat)) / (medval * len(residuals(pfinal[0], dat)))
    minicov = cov[0:2, 0:2]
    print minicov
#    minicov[1, 0] = 10.
#    minicov[0, 1] = 10.
    det = lin.det(minicov)
    minicovinv = lin.inv(minicov)
    prefix = 1. / (2. * numpy.pi * numpy.sqrt(det))
    xvec,yvec = numpy.meshgrid(imsize[0], imsize[1])
    pvec = numpy.zeros((imsize[0], imsize[1]))
    for i in numpy.arange(imsize[0]):
        for j in numpy.arange(imsize[1]):
            xvec = numpy.array([float(i) - params[0], float(j) - params[1]])
            pvec[i,j] = prefix * numpy.exp( -.5 * numpy.dot(numpy.dot(numpy.transpose(xvec), minicovinv), xvec))
    print numpy.sum(pvec)
    plot.imshow(pvec)
    plot.xlabel('X position')
    plot.ylabel('Y position')
    plot.xlim([params[1] - 25, params[1] + 25])
    plot.ylim([params[0] - 25, params[0] + 25])
    plot.title('Covariance as a function of X and Y position')
    plot.colorbar()
    plot.show()

    fig, axes = plot.subplots(nrows = 1, ncols = 3)
    axes[0].imshow(dat, vmin = 0, vmax = 160)
    axes[0].set_title('Data')
    axes[1].imshow(tdgauss(pfinal[0]), vmin = 0, vmax = 160)
    axes[1].set_title('Fit')
    im = axes[2].imshow(dat - tdgauss(pfinal[0]), vmin = 0, vmax = 160)
    axes[2].set_title('Residuals')
    fig.subplots_adjust(right = .8)
    #thanks stackoverflow
    cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
    fig.colorbar(im, cax =  cbar_ax)
    plot.show()
    

    
    
